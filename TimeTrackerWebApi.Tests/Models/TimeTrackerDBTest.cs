﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeTrackerWebApi.Models;

namespace TimeTrackerWebApi.Tests.Models
{
    [TestClass]
    public class TimeTrackerDBTest
    {
        [TestMethod]
        public void TestGetConnectionString()
        {
            string conn = TimeTrackerDB.GetConnectionString();
            Assert.IsNotNull(conn);
        }
    }
}
