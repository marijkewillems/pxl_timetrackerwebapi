﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeTrackerWebApi.Models;
using System.Collections.Generic;

namespace TimeTrackerWebApi.Tests.Models
{
    [TestClass]
    public class ProjectDBTest
    {
        [TestMethod]
        public void TestGetProjects()
        {
            ProjectDB db = new ProjectDB();
            List<Project> result = db.GetProjects();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count > 0);

            Project first = result[0];
            Assert.IsNotNull(first.ProjectID);
            Assert.IsNotNull(first.ProjectName);
            Assert.IsNotNull(first.Description);
            Assert.IsNotNull(first.Deadline);
            Assert.IsNotNull(first.CreationDate);
            Assert.IsNotNull(first.ModificationDate);
        }

        [TestMethod]
        public void TestGetProjectById()
        {
            ProjectDB db = new ProjectDB();
            Project first = db.GetProjectById(1);
            Assert.IsNotNull(first);


            first = db.GetProjectById(-1); // should not exist
            Assert.IsNull(first);
        }
    }
}
