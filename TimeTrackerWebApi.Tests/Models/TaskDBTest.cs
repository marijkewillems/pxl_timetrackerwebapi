﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeTrackerWebApi.Models;

namespace TimeTrackerWebApi.Tests.Models
{
    [TestClass]
    public class TaskDBTest
    {
        [TestMethod]
        public void TestGetProjectById()
        {
            TaskDB db = new TaskDB();
            List<Task> result = db.GetTasksByProject(-1);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);

            result = db.GetTasksByProject(1);
            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.Count);
        }
    }
}
