﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace TimeTrackerWebApi.Models
{
    public class UserDB
    {
        public List<User> GetUsers()
        {
            List<User> list = new List<User>();
            string sql = "SELECT UserID, UserName, Password " +
                         "FROM Users";

            using (SqlConnection con = new SqlConnection(TimeTrackerDB.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr.Read())
                    {
                        User u = CreateUser(dr);
                        list.Add(u);
                    }
                    dr.Close();
                }
            }
            return list;
        }

        public User GetUserByName(string userName)
        {
            User u = null;
            string sql = "SELECT UserID, Password " +
                         "FROM Users " +
                         "WHERE UserName = @UserName";

            using (SqlConnection con = new SqlConnection(TimeTrackerDB.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {

                    cmd.Parameters.AddWithValue("UserName", userName);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dr.Read())
                    {
                        u = CreateUser(dr);
                        u.UserName = userName;
                    }
                    dr.Close();
                }
            }
            return u;
        }
        private User CreateUser(IDataReader dr)
        {
            User u = new User();
            u.UserID = Convert.ToInt32(dr["UserID"]);
            u.Password = dr["Password"].ToString();
            return u;
        }
    }
}