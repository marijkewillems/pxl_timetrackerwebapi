﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace TimeTrackerWebApi.Models
{
    public class UserTaskDB
    {
        public List<UserTask> GetUserTasksByProjectTask(int userId, int projectTaskId)
        {
            List<UserTask> list = new List<UserTask>();
            string sql = "SELECT CreationDate, Comments, StartTime, EndTime, ShortDescription " +
                         "FROM UserTasks " +
                         "WHERE TaskId = @ProjectTaskId " +
                         "AND UserId = @UserId ";

            using (SqlConnection con = new SqlConnection(TimeTrackerDB.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@UserId",userId);
                    cmd.Parameters.AddWithValue("@ProjectTaskId", projectTaskId);
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr.Read())
                    {
                        UserTask t = CreateUserTask(dr);
                        t.TaskId = projectTaskId;
                        t.UserId = userId;
                        list.Add(t);
                    }
                    dr.Close();
                }
            }
            return list;
        }
    
        private UserTask CreateUserTask(SqlDataReader dr)
        {
            UserTask t = new UserTask();
            t.Comments = dr["Comments"].ToString();
            t.CreationDate = dr["CreationDate"].ToString();
            t.StartTime = dr["StartTime"].ToString();
            t.EndTime = dr["EndTime"].ToString();
            t.ShortDescription = dr["ShortDescription"].ToString();
            return t;
        }
    }

}