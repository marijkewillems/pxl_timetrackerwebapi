﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrackerWebApi.Models
{
    public class Task
    {
        public int TaskID { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public int ProjectId { get; set; }
        public String Priority { get; set; }
       // public enum Priorities { LO, ME, HI }

    }
}