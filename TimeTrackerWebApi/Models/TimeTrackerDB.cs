﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace TimeTrackerWebApi.Models
{
    public class TimeTrackerDB
    {
        public static string GetConnectionString()
        {
            return ConfigurationManager
                .ConnectionStrings["TimeTrackerConnectionString"].ConnectionString;
        }
    }
}