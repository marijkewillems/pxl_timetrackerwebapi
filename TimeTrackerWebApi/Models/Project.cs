﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrackerWebApi.Models
{
    public class Project
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string Deadline { get; set; } // should be DueDate
        public string CreationDate { get; set; }
        public string ModificationDate { get; set; }
    }
}