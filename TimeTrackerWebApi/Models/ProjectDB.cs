﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TimeTrackerWebApi.Models
{
    public class ProjectDB
    {
        public List<Project> GetProjects()
        {
            List<Project> list = new List<Project>();
            string sql = "SELECT ProjectID, ProjectName, Description, DueDate, CreationDate, ModificationDate " +
                         "FROM Projects";

            using (SqlConnection con = new SqlConnection(TimeTrackerDB.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr.Read())
                    {
                        Project p = CreateProject(dr);
                        list.Add(p);
                    }
                    dr.Close();
                }
            }
            return list;
        }

        public Project GetProjectById(int id)
        {
            Project p = null;
            string sql = "SELECT ProjectID, ProjectName, Description, DueDate, CreationDate, ModificationDate " +
                         "FROM Projects " +
                         "WHERE ProjectID = @ProjectID";

            using (SqlConnection con = new SqlConnection(TimeTrackerDB.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("ProjectID", id);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dr.Read())
                    {
                        p = CreateProject(dr);
                    }
                    dr.Close();
                }
            }
            return p;
        }


        public List<Project> GetProjectsByUser(int userId)
        {
            List<Project> list = new List<Project>();
            string sql = "SELECT p.ProjectID, p.ProjectName, Description, DueDate, CreationDate, ModificationDate " +
                         "FROM Projects p, ProjectUsers pu " +
                         "WHERE pu.userId = @UserID " + 
                         "AND pu.projectID = p.ProjectID";

            using (SqlConnection con = new SqlConnection(TimeTrackerDB.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("UserID", userId);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr.Read())
                    {
                        Project p = CreateProject(dr);
                        list.Add(p);
                    }
                    dr.Close();
                }
            }
            return list;
        }

        private Project CreateProject(IDataReader dr)
        {
            Project p = new Project();
            p.ProjectID = Convert.ToInt32(dr["ProjectID"]);
            p.ProjectName = dr["ProjectName"].ToString();
            p.Description = dr["Description"].ToString();
            p.Deadline = dr["DueDate"].ToString();
            p.CreationDate = dr["CreationDate"].ToString();
            p.ModificationDate = dr["ModificationDate"].ToString();

            return p;
        }
    }
}