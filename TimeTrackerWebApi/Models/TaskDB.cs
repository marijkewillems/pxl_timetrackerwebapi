﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TimeTrackerWebApi.Models
{
    public class TaskDB
    {
        public List<Task> GetTasksByProject(int projectId)
        {
            List<Task> list = new List<Task>();
            string sql = "SELECT TaskID, Description, Remarks, Priority " +
                         "FROM Tasks " +
                         "WHERE ProjectID = @ProjectID";

            using (SqlConnection con = new SqlConnection(TimeTrackerDB.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@ProjectID", projectId);
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr.Read())
                    {
                        Task t = CreateTask(dr);
                        list.Add(t);
                    }
                    dr.Close();
                }
            }
            return list;
        }

        private Task CreateTask(SqlDataReader dr)
        {
            Task t = new Task();
            t.TaskID = Convert.ToInt32(dr["TaskID"]);
            t.Description = dr["Description"].ToString();
            t.Remarks = dr["Remarks"].ToString();
            t.Priority = dr["Priority"].ToString();
            return t;
        }
    }
}