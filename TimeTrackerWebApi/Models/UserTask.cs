﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrackerWebApi.Models
{
    public class UserTask
    {
        public int TaskId { get; set; }
        public int UserId { get; set; }
        public string CreationDate { get; set; }
        public string Comments { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ShortDescription { get; set; }
    }
}