﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeTrackerWebApi.Models;

namespace TimeTrackerWebApi.Controllers
{
    public class ProjectsController : ApiController
    {
        private ProjectDB data;

        public ProjectsController()
        {
            this.data = new ProjectDB();
        }

        // GET: api/projects
        public IEnumerable<Project> GetProjects()
        {
            return data.GetProjects();
        }
/*
        // GET: api/projects/1
        public Project GetProjectById(int id)
        {
            return data.GetProjectById(id);
        }
*/
        public IEnumerable<Project> GetProjectsByUser(int id)
        { 
            return data.GetProjectsByUser(id);
        }
    }
}
