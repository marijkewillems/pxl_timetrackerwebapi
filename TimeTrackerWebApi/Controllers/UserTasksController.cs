﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeTrackerWebApi.Models;

namespace TimeTrackerWebApi.Controllers
{
    public class UserTasksController : ApiController
    {
       private UserTaskDB data;

        public UserTasksController()
        {
            data = new UserTaskDB();
        }

        // GET: api/usertasks/1/1/1
        public IEnumerable<UserTask> GetUserTasksByProjectTask(int id, int projectTaskId)
        {
            return data.GetUserTasksByProjectTask(id, projectTaskId);
        }
    }
}
