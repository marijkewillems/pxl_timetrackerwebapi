﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeTrackerWebApi.Models;

namespace TimeTrackerWebApi.Controllers
{
    public class UsersController : ApiController
    {
        private UserDB data;

        public UsersController()
        {
            this.data = new UserDB();
        }

        // GET: api/users
        public IEnumerable<User> GetUsers()
        {
             return data.GetUsers();
        }

        // GET: api/users/name/name
        public User GetUserByName(string name)
        {
            return data.GetUserByName(name);
        }


    }
}
