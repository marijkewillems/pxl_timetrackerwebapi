﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeTrackerWebApi.Models;

namespace TimeTrackerWebApi.Controllers
{
    public class TasksController : ApiController
    {
        private TaskDB data;

        public TasksController()
        {
            data = new TaskDB();
        }

        // GET: api/tasks/1
        public IEnumerable<Task> GetTasksByProject(int id)
        {
            return data.GetTasksByProject(id);
        }
    }
}
