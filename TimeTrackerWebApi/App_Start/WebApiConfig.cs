﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace TimeTrackerWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();



            config.Routes.MapHttpRoute(
             name: "UsersApi",
             routeTemplate: "api/{controller}/name/{name}",
             defaults: new { name = RouteParameter.Optional }
 );

            config.Routes.MapHttpRoute(
    name: "DefaultApi",
    routeTemplate: "api/{controller}/{id}/{projectTaskId}/{projectId}",
    defaults: new { id = RouteParameter.Optional, projectTaskId = RouteParameter.Optional, projectId = RouteParameter.Optional }
);
        }
    }
}
