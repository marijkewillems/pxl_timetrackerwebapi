﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TimeTrackerWebApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
  name: "Default",
   url: "{controller}/{action}/{id}/{projectTaskId}",
  defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, projectTaskId = UrlParameter.Optional }
);

            routes.MapRoute(
name: "SpecificRoute",
url: "{controller}/{action}/{name}",
defaults: new { controller = "Home", action = "Index", name = UrlParameter.Optional }
);


        }
    }
}
